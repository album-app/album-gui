import unittest
from unittest import mock

from album.client.api import AlbumClient

from src.album.gui import AlbumGUI


class TestInteractiveGUI(unittest.TestCase):

    def test_gui(self):
        #mocks
        mock_client = mock.create_autospec(AlbumClient)
        mock_client.resolve.return_value = {
            "setup": {
                "group": "my-group",
                "name": "my-name",
                "version": "my-version",
                "title": "This is a solution",
                "description": "This solution does magical things.",
                "args": [
                    {
                        "name": "input1",
                        "description": "this is a string parameter",
                        "type": "string",
                        "required": True,
                    },
                    {
                        "name": "input2",
                        "description": "this is a file parameter",
                        "type": "file"
                    },
                    {
                        "name": "input3",
                        "description": "this is a directory parameter, it's required, also, this description is a bit longer, to force a line break at some point",
                        "type": "directory",
                    },
                    {
                        "name": "input4",
                        "description": "this is a boolean parameter",
                        "type": "boolean"
                    },
                    {
                        "name": "input5",
                        "description": "this is a float parameter",
                        "type": "float"
                    },
                    {
                        "name": "input5",
                        "description": "this is a double parameter",
                        "type": "double"
                    },
                    {
                        "name": "input5",
                        "description": "this is an integer parameter",
                        "type": "integer"
                    },
                ]
            }
        }
        mock_client.is_installed.return_value = False
        mock_client.run.return_value = {
            "id": "0"
        }
        mock_client.get_task_status.side_effect = [{"status": "RUNNING", "records": [
            {"msg": "heavy work....", "asctime": 0}
        ]}, {"status": "FINISHED", "records": [
            {"msg": "heavy work....", "asctime": 0},
            {"msg": "done.", "asctime": 1}
        ]}, {"status": "RUNNING", "records": [
            {"msg": "heavy work....", "asctime": 0}
        ]}, {"status": "FINISHED", "records": [
            {"msg": "heavy work....", "asctime": 0},
            {"msg": "done.", "asctime": 1}
        ]}]

        client = mock_client
        gui = AlbumGUI()
        solution = "solution.py"
        if solution:
            gui.launch_solution(client, solution)
        else:
            gui.launch(client)
        gui.dispose()
        client.dispose()


if __name__ == '__main__':
    TestInteractiveGUI().run()
